﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using AdvertisementRemover.Enums;

namespace AdvertisementRemover
{
    /// <summary>
    ///     Program part
    /// </summary>
    class Program
    {
        #region Constants

        private const string BlockingStart = "# Start blocking advertisements";
        private const string BlockingEnd = "# End blocking advertisements";

        private const string OnlineSourceLocation = "http://winhelp2002.mvps.org/hosts.txt";
        private static readonly string LocalFileName = Path.Combine(Environment.ExpandEnvironmentVariables(@"%TEMP%"), "AdvertisementRemover", "hosts.txt");
        private static readonly string TempHostFile = Path.Combine(Environment.ExpandEnvironmentVariables("%TEMP%"), "AdvertisementRemover", "hosts.tmp");
        private static readonly string HostsFileName = Path.Combine(Environment.ExpandEnvironmentVariables("%windir%"), "System32", "drivers", "etc", "hosts");

        #endregion Constants

        #region Private Methods

        /// <summary>
        ///     Entrance point of the console application
        /// </summary>
        private static void Main(string[] args)
        {
            var dir = Path.Combine(Environment.ExpandEnvironmentVariables(@"%TEMP%"), "AdvertisementRemover");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            Console.WriteLine($"Downloading file from '{OnlineSourceLocation}' - please wait...");

            if (!DownloadFile())
                StopExecution(ExitCodes.DownloadFailed);

            var fileInfo = new FileInfo(LocalFileName);
            Console.WriteLine($"Dowload file succeeded - Size: {fileInfo.Length} bytes");

            var entries = GetFileEntries();
            if (entries == null || !entries.Any())
            {
                PrintMessage("Could not find any entry...", ConsoleColor.Yellow);
                StopExecution(ExitCodes.NoEntry);
                return;
            }

            PrintMessage($"{entries.Count} entries found", Console.ForegroundColor);

            CreateTempHostFile();
            FillEntries(entries);
            ReplaceOldHostFile();

            DeleteTemporaryFiles();

            PrintMessage("Advertisements were successfully blocked.", ConsoleColor.DarkGreen);
            StopExecution(ExitCodes.Succeeded);
        }

        /// <summary>
        ///     Loading the hosts entries from web source and store it as text file on the local computer
        /// </summary>
        /// <returns>True if the download was succeeded</returns>
        private static bool DownloadFile()
        {
            using (var wc = new WebClient())
            {
                try
                {
                    wc.DownloadFile(OnlineSourceLocation, LocalFileName);

                    return true;
                }
                catch (Exception ex)
                {
                    PrintMessage(ex);
                    return false;
                }
            }
        }

        /// <summary>
        ///     Analyzes the downloaded file and store entries in a list
        /// </summary>
        /// <returns>List with all entries in the file</returns>
        private static List<string> GetFileEntries()
        {
            if (!File.Exists(LocalFileName))
            {
                PrintMessage("File doees not exists!");
                StopExecution(ExitCodes.FileCouldNotBeFound);
                return null;
            }

            PrintMessage("Loading entries from downloaded file ...", Console.ForegroundColor);

            const string startDetection = "# [Start of entries";
            const string endDetection = "# [end of entries";

            try
            {
                var advertisementEntries = new List<string>();

                using (var sw = new StreamReader(LocalFileName))
                {
                    var foundStartStr = false;

                    string line;
                    while ((line = sw.ReadLine()) != null)
                    {
                        if (!foundStartStr)
                        {
                            if (line.StartsWith(startDetection))
                                foundStartStr = true;

                            continue;
                        }

                        if (line.StartsWith(endDetection))
                            continue;

                        advertisementEntries.Add(line);
                    }
                }

                return advertisementEntries;
            }
            catch (Exception ex)
            {
                PrintMessage(ex);
                StopExecution(ExitCodes.ErrorLoadingFileContent);
                return null;
            }
        }

        /// <summary>
        ///     Creating a temporary host file with the already existing entries in host (above from this programm generated part)
        /// </summary>
        private static void CreateTempHostFile()
        {
            PrintMessage("Create temporary host file", Console.ForegroundColor);

            using (var sw = new StreamWriter(TempHostFile))
            {
                using (var sr = new StreamReader(HostsFileName))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.StartsWith(BlockingStart))
                            break;

                        sw.WriteLine(line);
                    }
                }
            }
            
        }

        /// <summary>
        ///     Insert all stored entries to the temporary hosts file
        /// </summary>
        /// <param name="entries">Entries that should be written to file</param>
        private static void FillEntries(List<string> entries)
        {
            PrintMessage("Write entries to temporary host file", Console.ForegroundColor);

            using (var sw = new StreamWriter(TempHostFile, true))
            {
                sw.WriteLine(BlockingStart);
                sw.WriteLine($"# File generated by AdvertisementRemover - ad-list from {OnlineSourceLocation}");
                sw.WriteLine("# ATTENTION! If you add other entries, enter above this lines!");

                foreach (var entry in entries)
                {
                    sw.WriteLine(entry);
                }

                sw.WriteLine(BlockingEnd);
            }
        }

        /// <summary>
        ///     Copies the temporary host file to the system folder and replace the systems file
        /// </summary>
        private static void ReplaceOldHostFile()
        {
            PrintMessage("Copy temporary host file to system folder", Console.ForegroundColor);

            File.Copy(TempHostFile, HostsFileName, true);
        }

        /// <summary>
        ///     Removes all temporary files
        /// </summary>
        private static void DeleteTemporaryFiles()
        {
            PrintMessage("Delete temporary files", Console.ForegroundColor);

            File.Delete(TempHostFile);
            File.Delete(LocalFileName);
        }

        #endregion Private Methods

        #region Helper methods

        private static void PrintMessage(object msg, ConsoleColor color = ConsoleColor.Red)
        {
            var originalColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(msg);
            Console.ForegroundColor = originalColor;
        }

        private static void StopExecution(ExitCodes exitCode)
        {
            Console.WriteLine("Press any key for exit ...");
            Console.ReadKey();
            Environment.Exit((int)exitCode);
        }

        #endregion Helper methods
    }
}
