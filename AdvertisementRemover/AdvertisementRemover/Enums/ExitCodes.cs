﻿namespace AdvertisementRemover.Enums
{
    /// <summary>
    ///     All exit codes that are used in this software
    /// </summary>
    public enum ExitCodes
    {
        /// <summary>
        ///     Successfully finished
        /// </summary>
        Succeeded = 0x0,

        /// <summary>
        ///     Error while downloading
        /// </summary>
        DownloadFailed = 0x1,

        /// <summary>
        ///     Downloaded file was not found
        /// </summary>
        FileCouldNotBeFound = 0x2,

        /// <summary>
        ///     File content of downloaded file could not be loaded
        /// </summary>
        ErrorLoadingFileContent = 0x3,

        /// <summary>
        ///     No entry could be found
        /// </summary>
        NoEntry = 0x4,
    }
}
