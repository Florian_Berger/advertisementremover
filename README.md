# Advertisement Remover

You have enough of advertisement in Windows� software (e.g. Skype)? Then, THIS is the perfect tool for you!

##### What is this software doing?
The program downloads a file with over 4,000 advertisement hosts (list found [here](http://winhelp2002.mvps.org/hosts.htm)) and add it to the host file. All requests to this hosts will be redirected to 127.0.0.1, so no longer you will see ads (also in sofware out of the browser!).

##### How to use it?
All you have to do is executing the **AdvertisementRemover.exe**. All other steps will be done automatically.

##### Where to find the download?
You can find all versions on the [Downloads page](https://bitbucket.org/Florian_Berger/advertisementremover/downloads/)



##### License
This source is released under **BSD License**. You can find the complete license text in the **LICENSE.md** file.